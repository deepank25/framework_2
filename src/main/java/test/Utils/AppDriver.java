package test.Utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class AppDriver {
	public static final String CONFIG="./src/main/config/configuration.properties";
	public Properties prop;
	
	public String reval;
	public Properties loadProperty(String filepath)
	{ 
		prop= new Properties();
		try
		{
			FileInputStream file = new FileInputStream(filepath);
			prop.load(file);
		}catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("fILE nOT fOUND AT"+filepath);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return prop;
	}
	
	public String fetchData(String locator)
	{ reval="";
	String filepath= loadProperty(CONFIG).getProperty("Locators");
	try
		{
			
			File file = new File(filepath);
			DocumentBuilder db= DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Document doc=db.parse(file);
			
			doc.getDocumentElement().normalize();
			
			NodeList nlist= doc.getElementsByTagName("element");
			
			for(int i=0;i<nlist.getLength();i++)
			{ Node nNode=nlist.item(i);
			if(nNode.getNodeType()==Node.ELEMENT_NODE)
			{Element eElement= (Element)nNode;
			if(eElement.getAttribute("name").contains(locator))
			{
				reval=eElement.getElementsByTagName("locator").item(0).getTextContent();
			}
				
			}
				
			}
			
		}
	catch (Exception e) {
		e.printStackTrace();
		System.out.println(" Locators File not found at :"+filepath);
		}
		
		return reval;
	}
	public NodeList readTestData()
	{	NodeList Steps=null;
		String filepath=loadProperty(CONFIG).getProperty("TestData");
		try
		{
			File file=new File(filepath);
			DocumentBuilder db= DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Document doc=db.parse(file);
			doc.getDocumentElement().normalize();
			
			NodeList nlist= doc.getElementsByTagName("TestCaseName");
			
			for(int i=0;i<nlist.getLength();i++)
			{ Node nNode=nlist.item(i);
			if(nNode.getNodeType()==Node.ELEMENT_NODE)
			{Element eElement= (Element)nNode;
			if(eElement.getAttribute("execute").contains("yes"))
			{
				Steps= eElement.getElementsByTagName("stepname");
			}
				
			}
				
			}
			
		}catch (Exception e) {
			// TODO: handle exception
		}
		return Steps;
	}
	public void writeSteps()
	{
		NodeList Steps=readTestData();
		for(int i=0;i<Steps.getLength();i++)
		{ Node nNode=Steps.item(i);
		if (nNode.getNodeType()==Node.ELEMENT_NODE) {
			Element eElement= (Element)nNode;
			System.out.println("Step Name :"+eElement.getAttribute("name"));
			
		}
			
		}
	}
	public void testCaseRunner()
	{ String className=loadProperty(CONFIG).getProperty("TestClass");
	NodeList Steps=readTestData();
	TestEngine engine = new TestEngine(className);
	for(int i=0;i<Steps.getLength();i++)
		{ HashMap<String,String> input= new HashMap<String, String>();
			Node nNode=Steps.item(i);
			
	if (nNode.getNodeType()==Node.ELEMENT_NODE) {
		Element eElement= (Element)nNode;
		NodeList Stepdata=eElement.getChildNodes();
		for(int j=0;j<Stepdata.getLength();j++)
		{
			Node data=Stepdata.item(j);
			
		if (data.getNodeType()==Node.ELEMENT_NODE) {
			Element stepele= (Element)data;
			
			input.put(stepele.getTagName(),stepele.getTextContent());
			//System.out.println(stepele.getTagName()+stepele.getTextContent());
		}
		
	}
	}
	engine.runtest(input.get("actioncode"), input);
	}
		
	}
}
