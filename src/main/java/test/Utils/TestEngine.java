package test.Utils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;

public class TestEngine {
	
	ClassLoader loader= null;
	Class<?> clazz= null;
	Object classObj =null;
	
	
	public TestEngine(String className)  {
		 loader= this.getClass().getClassLoader();
		 try {
			clazz= loader.loadClass(className);
			classObj = clazz.newInstance();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	 
	public  void runtest( String actioncode,HashMap<String, String> input)
	{
	
	try {

		for(Method method:clazz.getMethods())
		{
			if(method.getName().contains(actioncode))
			{
				method.invoke(classObj, input);
			}
		}
		
	} catch (IllegalAccessException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IllegalArgumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (InvocationTargetException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} 
	}
}
