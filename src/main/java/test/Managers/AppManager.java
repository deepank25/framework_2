package test.Managers;

import test.Utils.AppDriver;

public class AppManager {
 
	private static AppManager appManager;
	private static AppDriver appDriver;
	
	private AppManager() {}
	
	public static AppManager getiInstance()
	{
		return (appManager == null)?appManager= new AppManager():appManager;	
	}
	
	public AppDriver getAppDriver()
	{
		return (appDriver == null)?appDriver= new AppDriver():appDriver;
	}
	
	
}
