package test.Drone;

import java.util.HashMap;

import org.openqa.selenium.WebDriver;

import test.Managers.DriverManager;

/**
 * Hello world!
 *
 */
public class App 
{
	WebDriver driver = null;
	
	
	public void method_1(HashMap<String,String>input)
	{
		System.out.println("Executing method 1-------->\n"+"UserName : "+input.get("username"));
		driver = new DriverManager().getDriver();
		driver.get("https:\\www.flipkart.com");
	}
	public void method_2(HashMap<String,String>input)
	{
		System.out.println("Executing method 2-------->\n"+"UserName : "+input.get("username"));
		driver.navigate().to("https:\\www.gmail.com");
	}
	public void method_3(HashMap<String,String>input)
	{
		System.out.println("Executing method 3-------->\n"+"UserName : "+input.get("username"));
		driver.close();
	}
	public void method_4(HashMap<String,String>input)
	{
		System.out.println("Executing method 4-------->\n"+"UserName : "+input.get("username"));
	}
	public void method_5(HashMap<String,String>input)
	{
		System.out.println("Executing method 5-------->\n"+"UserName : "+input.get("username"));
	}
	public void method_6(HashMap<String,String>input)
	{
		System.out.println("Executing method 6-------->\n"+"UserName : "+input.get("username"));
	}
	
}
